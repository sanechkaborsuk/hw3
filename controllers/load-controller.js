/* eslint-disable camelcase */
const {Load} = require('../models/load-model');
const {Truck} = require('../models/truck-model');

const LoadDao = require('../models/dao/load-dao');
const {findTruckType} = require('../models/dao/find-suitable-truck-type');

const {BadRequest} = require('../routers/middlewares/errors/error-types');
const {requiredFields, noLoad, wrongFields, bigLoad, noTruck,
  wrongLoadStatus} = require('../routers/middlewares/errors/error-messages');

module.exports.getAllUserLoads = async (request, response, next) => {
  try {
    const {status,
      limit=10, offset=0} = request.query;
    const user = request.user;
    let userLoads = [];
    if (user.role ==='SHIPPER') {
      userLoads = await Load.find(
          {created_by: request.user._id},
          [],
          {
            status: status,
            limit: Number(limit),
            skip: Number(offset),
          },
      );
    } else if (user.role ==='DRIVER') {
      userLoads = await Load.find(
          {assigned_to: request.user._id},
          [],
          {
            status: status,
            limit: Number(limit),
            skip: Number(offset),
          },
      );
    }

    response.status(200).json({loads: userLoads});
  } catch (error) {
    next(error);
  }
};

module.exports.addNewLoad = async (request, response, next) => {
  try {
    const {name, payload,
      pickup_address, delivery_address,
      dimensions: {width, length, height}} = request.body;

    if (!name || !payload || !pickup_address ||
        !delivery_address || !width || !length || !height) {
      throw new BadRequest(requiredFields);
    }

    const newLoad = new Load({
      created_by: request.user._id,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {
        width,
        length,
        height,
      },
    });
    await newLoad.save();
    response.status(200).json({message: 'Load created successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.getActiveLoad = async (request, response, next) => {
  try {
    const activeLoad = await Load.findOne({
      assigned_to: request.user._id, status: 'ASSIGNED'});
    if (!activeLoad) throw new BadRequest(noLoad);

    response.status(200).json({load: activeLoad});
  } catch (error) {
    next(error);
  }
};

module.exports.changeLoadState = async (request, response, next) => {
  try {
    const activeLoad = await Load.findOne({
      assigned_to: request.user._id, status: 'ASSIGNED'});
    if (!activeLoad) throw new BadRequest(noLoad);

    const truck = await Truck.findOne({assigned_to: request.user._id});

    let newState;
    if (activeLoad.state == 'En route to delivery') {
      newState = 'Arrived to delivery';
      activeLoad.state = newState;
      activeLoad.status = 'SHIPPED';
      activeLoad.logs.push({message: `Load ${newState}`, time: Date.now()});
      activeLoad.save();

      truck.status = null;
      truck.assigned_to = null;
      truck.save();
    } else if (activeLoad.state == 'Arrived to Pick Up') {
      newState = 'En route to delivery';
      activeLoad.state = newState;
      activeLoad.logs.push({message: `Load ${newState}`, time: Date.now()});
      activeLoad.save();
    } else if (activeLoad.state == 'En route to Pick Up') {
      newState = 'Arrived to Pick Up';
      activeLoad.state = newState;
      activeLoad.logs.push({message: `Load ${newState}`, time: Date.now()});
      activeLoad.save();
    }

    response.status(200).json({message: `Load state changed to ${newState}`});
  } catch (error) {
    next(error);
  }
};

module.exports.getLoadById = async (request, response, next) => {
  try {
    const load = await Load.findOne({
      _id: request.params.id, created_by: request.user._id});
    if (!load) throw new BadRequest(noLoad);

    response.status(200).json({load: load});
  } catch (error) {
    next(error);
  }
};

const availableToUpdate = ['name', 'payload', 'pickup_address',
  'delivery_address', 'width', 'length', 'height'];

module.exports.updateLoadById = async (request, response, next) => {
  try {
    const needUpdate = [];
    for (const item of Object.keys(request.body)) {
      if (typeof request.body[item] === 'object') {
        const nested = Object.keys(request.body[item]);
        needUpdate.push(...nested);
      } else {
        needUpdate.push(item);
      }
    }
    const isAvailable = needUpdate.every(
        (item) => availableToUpdate.includes(item));
    if (!isAvailable) throw new BadRequest(wrongFields);

    const load = await Load.findOne({
      _id: request.params.id, created_by: request.user._id, status: 'NEW'});
    if (!load) throw new BadRequest(noLoad);

    needUpdate.forEach((item) => {
      if (item === 'width' || item === 'length' || item === 'height') {
        load.dimensions[item] = request.body.dimensions[item];
      } else {
        load[item] = request.body[item];
      }
    });
    await load.save();
    response.status(200).json({message: 'Load details changed successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.deleteLoadById = async (request, response, next) => {
  try {
    const deleteLoad = await Load.findOneAndDelete({_id: request.params.id,
      created_by: request.user._id, status: 'NEW'});
    if (!deleteLoad) throw new BadRequest(noLoad);

    response.status(200).json({message: 'Load deleted successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.searchDriverForLoad = async (request, response, next) => {
  try {
    const load = await LoadDao.findLoadByIdAndShipper(
        request.params.id, request.user._id);
    if (load.status != 'NEW') throw new BadRequest(wrongLoadStatus);

    const truckType = findTruckType(load);
    if (!truckType) throw new BadRequest(bigLoad);

    const perfectTruck = await Truck.findOne({type: truckType, status: 'IS'});
    // ! Or res-200 can not found truck now, try later ?!
    if (!perfectTruck) throw new BadRequest(noTruck);

    load.status = 'ASSIGNED',
    load.state = 'En route to Pick Up',
    load.assigned_to = perfectTruck.assigned_to,
    load.logs.push({
      message: `Load assigned to driver with id ${perfectTruck.assigned_to}`,
      time: Date.now(),
    });
    await load.save();

    perfectTruck.status = 'OL',
    await perfectTruck.save();

    response.status(200).json({message: 'Load posted successfully',
      driver_found: true});
  } catch (error) {
    next(error);
  }
};

module.exports.getLoadInfoById = async (request, response, next) => {
  try {
    const load = await LoadDao.findLoadByIdAndShipper(
        request.params.id, request.user._id);
    const truck = await Truck.findOne({created_by: load.assigned_to});

    response.status(200).json({load: load, truck: truck});
  } catch (error) {
    next(error);
  }
};
