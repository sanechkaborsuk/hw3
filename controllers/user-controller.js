const bcrypt = require('bcrypt');
const {User} = require('../models/user-model');

const {BadRequest} = require('../routers/middlewares/errors/error-types');
const {wrongPassword} = require('../routers/middlewares/errors/error-messages');

module.exports.getProfile = (request, response, next) => {
  try {
    response.status(200).json({user: {
      _id: request.user._id,
      email: request.user.email,
      created_date: request.user.created_date,
    }});
  } catch (error) {
    next(error);
  }
};

module.exports.deleteProfile = async (request, response, next) => {
  try {
    await User.findByIdAndDelete(request.user._id);
    response.status(200).json({message: 'Delete user successfully!'});
  } catch (error) {
    next(error);
  }
};

module.exports.changePassword = async (request, response, next) => {
  try {
    const {oldPassword, newPassword} = request.body;
    const user = await User.findOne({_id: request.user._id});

    if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
      throw new BadRequest(wrongPassword);
    }

    await User.findByIdAndUpdate(user._id,
        {$set: {password: await bcrypt.hash(newPassword, 5)}});

    response.status(200).json({message: 'Change password successfully!'});
  } catch (error) {
    next(error);
  }
};
