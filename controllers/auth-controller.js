const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/user-model');
const {BadRequest} = require('../routers/middlewares/errors/error-types');
const {
  requiredFields,
  noUser,
  wrongPassword,
} = require('../routers/middlewares/errors/error-messages');

module.exports.register = async (request, response, next) => {
  try {
    const {email, password, role} = request.body;
    const user = new User({
      email,
      password: await bcrypt.hash(password, 5),
      role,
    });

    await user.save();
    response.status(200).json({message: 'Create successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.login = async (request, response, next) => {
  try {
    const {email, password} = request.body;
    if (!email || !password) throw new BadRequest(requiredFields);

    const user = await User.findOne({email});
    if (!user) throw new BadRequest(noUser);

    if ( !(await bcrypt.compare(password, user.password)) ) {
      throw new BadRequest(wrongPassword);
    }

    const token = jwt.sign({
      _id: user._id,
      email: user.email,
      created_date: user.created_date,
      role: user.role,
    }, JWT_SECRET);

    response.status(200).json({jwt_token: token});
  } catch (error) {
    next(error);
  }
};

module.exports.forgotPassword = async (request, response, next) => {
  try {
    const {email} = request.body;
    if (!email) throw new BadRequest(requiredFields);

    const user = await User.findOne({email});
    if (!user) throw new BadRequest(noUser);

    response.status(200).json({
      message: 'New password sent to your email address'});
  } catch (error) {
    next(error);
  }
};
