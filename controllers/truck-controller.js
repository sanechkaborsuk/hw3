const {Truck} = require('../models/truck-model');
const TruckDao = require('../models/dao/truck-dao');

const {BadRequest} = require('../routers/middlewares/errors/error-types');
const {
  requiredFields,
  noTruck} = require('../routers/middlewares/errors/error-messages');

module.exports.getAllUserTrucks = async (request, response, next) => {
  try {
    const userTrucks = await Truck.find({created_by: request.user._id});
    response.status(200).json({trucks: userTrucks});
  } catch (error) {
    next(error);
  }
};

module.exports.addNewTruck = async (request, response, next) => {
  try {
    const {type} = request.body;
    if (!type) throw new BadRequest(requiredFields);

    const newTruck = new Truck({
      created_by: request.user._id,
      type: type,
    });

    await newTruck.save();
    response.status(200).json({message: 'Truck created successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.getTruckById = async (request, response, next) => {
  try {
    const userTruck = await Truck.findOne({
      _id: request.params.id, created_by: request.user._id});
    if (!userTruck) throw new BadRequest(noTruck);
    response.status(200).json({truck: userTruck});
  } catch (error) {
    next(error);
  }
};

module.exports.updateTruckById = async (request, response, next) => {
  try {
    const truckId = request.params.id;
    const {type} = request.body;
    if (!type) throw new BadRequest(requiredFields);

    await TruckDao.checkTruckStatus(truckId, request.user._id);

    await Truck.findByIdAndUpdate(truckId, {$set: {type: type}});
    response.status(200).json({message: 'Truck details changed successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.deleteTruckById = async (request, response, next) => {
  try {
    const truckId = request.params.id;
    await TruckDao.checkTruckStatus(truckId, request.user._id);

    await Truck.findByIdAndDelete(truckId);
    response.status(200).json({message: 'Truck deleted successfully'});
  } catch (error) {
    next(error);
  }
};

module.exports.assignTruckToUserById = async (request, response, next) => {
  try {
    const truckId = request.params.id;
    await TruckDao.checkTruckStatus(truckId, request.user._id);

    await Truck.findByIdAndUpdate(truckId,
        {$set: {assigned_to: request.user._id, status: 'IS'}});
    response.status(200).json({message: 'Truck assigned successfully'});
  } catch (error) {
    next(error);
  }
};
