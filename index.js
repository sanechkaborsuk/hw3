const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 8080;

require('dotenv').config();

const authRouter = require('./routers/auth-router');
const userRouter = require('./routers/user-router');
const truckRouter = require('./routers/truck-router');
const loadRouter = require('./routers/load-router');

const app = express();

app.use(express.json());
app.use(morgan('combined'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://hw:test@cluster0.s0nrs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT);
};

start();
