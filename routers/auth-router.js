const express = require('express');
const router = new express.Router();

const handleErrors = require('./middlewares/errors/error-handling');
const {validateRegistration} = require('./middlewares/validation-middleware');
const {
  register,
  login,
  forgotPassword} = require('../controllers/auth-controller');

router.post('/register', validateRegistration, register);
router.post('/login', login);
router.post('/forgot_password', forgotPassword);

router.use(handleErrors);

module.exports = router;
