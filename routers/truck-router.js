const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/auth-middleware');
const {isDriverMiddleware} = require('./middlewares/check-role-middleware');
const {validateCreateNewTruck} = require('./middlewares/validation-middleware');
const handleErrors = require('./middlewares/errors/error-handling');
const {
  getAllUserTrucks,
  addNewTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckToUserById,
} = require('../controllers/truck-controller');

router.use(authMiddleware);
router.use(isDriverMiddleware);

router.get('/', getAllUserTrucks);
router.post('/', validateCreateNewTruck, addNewTruck);
router.get('/:id', getTruckById);
router.put('/:id', validateCreateNewTruck, updateTruckById);
router.delete('/:id', deleteTruckById);
router.post('/:id/assign', assignTruckToUserById);

router.use(handleErrors);

module.exports = router;
