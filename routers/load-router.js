const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/auth-middleware');
const {validateCreateNewLoad} = require('./middlewares/validation-middleware');
const {isDriverMiddleware,
  isShipperMiddleware} = require('./middlewares/check-role-middleware');
const handleErrors = require('./middlewares/errors/error-handling');
const {
  getAllUserLoads, addNewLoad, getActiveLoad,
  changeLoadState, getLoadById, updateLoadById,
  deleteLoadById, searchDriverForLoad, getLoadInfoById,
} = require('../controllers/load-controller');

router.use(authMiddleware);

router.get('/', getAllUserLoads);
router.post('/', isShipperMiddleware, validateCreateNewLoad, addNewLoad);
router.get('/active', isDriverMiddleware, getActiveLoad);
router.patch('/active/state', isDriverMiddleware, changeLoadState);
router.get('/:id', isShipperMiddleware, getLoadById);
router.put('/:id', isShipperMiddleware, updateLoadById);
router.delete('/:id', isShipperMiddleware, deleteLoadById);
router.post('/:id/post', isShipperMiddleware, searchDriverForLoad);
router.get('/:id/shipping_info', isShipperMiddleware, getLoadInfoById);

router.use(handleErrors);

module.exports = router;
