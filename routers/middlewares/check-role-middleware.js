const {User} = require('../../models/user-model');
const {BadRequest} = require('./errors/error-types');
const {wrongRole} = require('./errors/error-messages');

module.exports.isShipperMiddleware = async (request, response, next) => {
  try {
    const user = await User.findOne({_id: request.user._id});
    if (user.role != 'SHIPPER') throw new BadRequest(wrongRole);
    next();
  } catch (error) {
    next(error);
  }
};

module.exports.isDriverMiddleware = async (request, response, next) => {
  try {
    const user = await User.findOne({_id: request.user._id});
    if (user.role != 'DRIVER') throw new BadRequest(wrongRole);
    next();
  } catch (error) {
    next(error);
  }
};
