const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

const {BadRequest} = require('./errors/error-types');
const {
  authProblem,
  tokenProblem} = require('./errors/error-messages');

module.exports.authMiddleware = (request, response, next) => {
  const header = request.headers['authorization'];
  if (!header) throw new BadRequest(authProblem);

  const token = header.split(' ').pop();
  if (!token) throw new BadRequest(tokenProblem);

  request.user = jwt.verify(token, JWT_SECRET);
  next();
};
