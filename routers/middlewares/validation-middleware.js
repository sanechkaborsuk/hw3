const joi = require('joi');

module.exports.validateRegistration = async (request, response, next) => {
  try {
    const schema = joi.object({
      email: joi.string().email().required(),
      password: joi.string()
          .pattern(new RegExp('^[a-zA-Z0-9]{1,30}$')).required(),
      role: joi.string().valid('SHIPPER', 'DRIVER').required(),
    });

    await schema.validateAsync(request.body);
    next();
  } catch (error) {
    next(error);
  }
};

module.exports.validateCreateNewTruck = async (request, response, next) => {
  try {
    const schema = joi.object({
      type: joi.string().valid('SPRINTER',
          'SMALL STRAIGHT', 'LARGE STRAIGHT').required(),
    });

    await schema.validateAsync(request.body);
    next();
  } catch (error) {
    next(error);
  }
};

module.exports.validateCreateNewLoad = async (request, response, next) => {
  try {
    const schema = joi.object({
      name: joi.string().required(),
      payload: joi.number().required(),
      pickup_address: joi.string().required(),
      delivery_address: joi.string().required(),
      dimensions: {
        width: joi.number().required(),
        length: joi.number().required(),
        height: joi.number().required(),
      },
    });

    await schema.validateAsync(request.body);
    next();
  } catch (error) {
    next(error);
  }
};
