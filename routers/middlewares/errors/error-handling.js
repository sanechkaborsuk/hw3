const {GeneralError} = require('../errors/error-types');

const handleErrors = (err, request, response, next) => {
  if (err instanceof GeneralError) {
    return response.status(err.getCode()).json({
      status: 'error',
      message: err.message,
    });
  }

  return response.status(500).json({
    status: 'error',
    message: err.message,
  });
};

module.exports = handleErrors;
