const express = require('express');
const router = new express.Router();

const handleErrors = require('./middlewares/errors/error-handling');
const {authMiddleware} = require('./middlewares/auth-middleware');
const {isShipperMiddleware} = require('./middlewares/check-role-middleware');
const {
  getProfile,
  deleteProfile,
  changePassword} = require('../controllers/user-controller');

router.use(authMiddleware);

router.get('/', getProfile);
router.delete('/', isShipperMiddleware, deleteProfile);
router.patch('/password', changePassword);

router.use(handleErrors);

module.exports = router;
