const {Load} = require('../load-model');
const {BadRequest} = require('../../routers/middlewares/errors/error-types');
const {noLoad} = require('../../routers/middlewares/errors/error-messages');

module.exports.findLoadByIdAndShipper = async (id, shipper) => {
  const load = await Load.findOne({_id: id, created_by: shipper});
  if (!load) throw new BadRequest(noLoad);

  return load;
};
