const {Truck} = require('../truck-model');

const {BadRequest} = require('../../routers/middlewares/errors/error-types');
const {loadTruck,
  noTruck} = require('../../routers/middlewares/errors/error-messages');

module.exports.checkTruckStatus = async (truckId, owner) => {
  const userTruck = await Truck.findOne({_id: truckId, created_by: owner});
  if (!userTruck) throw new BadRequest(noTruck);
  if (userTruck.assigned_to == owner) throw new BadRequest(loadTruck);
  return userTruck;
};

