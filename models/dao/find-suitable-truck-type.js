module.exports.findTruckType = (load) => {
  let type;
  if (Number(load.payload) < 1700 &&
  Number(load.dimensions.width) < 300 &&
  Number(load.dimensions.length) < 250 &&
  Number(load.dimensions.height) < 170) {
    return type = 'SPRINTER';
  } else if (Number(load.payload) < 2500 &&
  Number(load.dimensions.width) < 500 &&
  Number(load.dimensions.length) < 250 &&
  Number(load.dimensions.height) < 170) {
    return type = 'SMALL STRAIGHT';
  } else if (Number(load.payload) < 4000 &&
  Number(load.dimensions.width) < 700 &&
  Number(load.dimensions.length) < 350 &&
  Number(load.dimensions.height) < 200) {
    return type = 'LARGE STRAIGHT';
  }
  return type;
};
